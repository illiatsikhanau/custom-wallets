export default interface Product {
    id: number
    src: string
    name: string
}
