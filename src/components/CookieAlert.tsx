'use client'

import '@/styles/CookieAlert.scss';
import {useEffect, useState} from 'react';
import Button from '@/components/Button';
import Link from 'next/link';

const CookieAlert = () => {
    const [hidden, setHidden] = useState(true);

    const acceptCookie = () => {
        localStorage.setItem('cookieAccepted', 'true');
        setHidden(true);
    }

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const cookieAccepted = !!localStorage.getItem('cookieAccepted');
            if (!cookieAccepted) {
                setHidden(false);
            }
        }
    }, []);

    return (
        <>
            {hidden ? '' :
                <div className='cookie-alert-wrapper'>
                    <div className='cookie-alert'>
                <span className='cookie-alert-message'>
                    By using this site, you agree to
                    our <Link href='/policy#cookie' className='cookie-alert-link'>Cookie Policy</Link>
                </span>
                        <div className='cookie-alert-options'>
                            <Button onClick={acceptCookie}>Accept</Button>
                            <span className='decline' onClick={acceptCookie}>Decline</span>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}

export default CookieAlert;
