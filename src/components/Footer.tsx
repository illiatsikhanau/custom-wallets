import '@/styles/Footer.scss';
import Link from 'next/link';

const Footer = () => {
    return (
        <footer className='footer-wrapper'>
            <ul className='footer-menu'>
                <li className='footer-menu-element'>
                    <Link className='footer-menu-element-link' href='/'>Home</Link>
                </li>
                <li className='footer-menu-element'>
                    <Link className='footer-menu-element-link' href='/crafting'>Crafting</Link>
                </li>
                <li className='footer-menu-element'>
                    <Link className='footer-menu-element-link' href='/collection'>Collection</Link>
                </li>
                <li className='footer-menu-element'>
                    <Link className='footer-menu-element-link' href='/materials'>Materials</Link>
                </li>
                <li className='footer-menu-element'>
                    <Link className='footer-menu-element-link' href='/about'>About us</Link>
                </li>
                <li className='footer-menu-element'>
                    <Link className='footer-menu-element-link' href='/policy'>Privacy policy</Link>
                </li>
            </ul>
        </footer>
    );
}

export default Footer;
