import '@/styles/Product.scss';
import Button from '@/components/Button';
import Product from '@/interfaces/Product';

const Product = ({product}: { product: Product }) => {
    return (
        <div className='product'>
            <div className='product-banner'>
                <div className='product-image' style={{backgroundImage: `url('${product.src}')`}}>
                    <div className='banner-filter'>
                        <Button href='/#order-form'>Order Now</Button>
                    </div>
                </div>
            </div>
            <p className='product-name'>{product.name}</p>
        </div>
    );
}

export default Product;
