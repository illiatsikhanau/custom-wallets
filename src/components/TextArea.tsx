import '@/styles/Input.scss';

interface Props {
    className?: string
    label?: string
}

const Input = (props: Props) => {
    return (
        <label className={`input-wrapper ${props.className ?? ''}`}>
            <span className='wrapper-label'>{props.label}</span>
            <textarea className='input-field' rows={5}/>
        </label>
    );
}

export default Input;
