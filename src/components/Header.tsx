import '@/styles/Header.scss';
import Link from 'next/link';
import Image from 'next/image';

import logo from '../../public/logo.svg';
import logoShort from '../../public/logo-short.svg';

const Header = () => {
    return (
        <header className='header'>
            <ul className='header-menu'>
                <li className='header-menu-element crafting'>
                    <Link className='header-menu-link' href='/crafting'>
                        Crafting
                    </Link>
                </li>
                <li className='header-menu-element collection'>
                    <Link className='header-menu-link' href='/collection'>
                        Collection
                    </Link>
                </li>
                <li className='header-menu-element'>
                    <Link className='header-menu-link' href='/'>
                        <Image className='logo hide-xl' src={logoShort} alt='Personalized Wallet'/>
                        <Image className='logo hidden show-xl' src={logo} alt='Personalized Wallet'/>
                    </Link>
                </li>
                <li className='header-menu-element materials hidden show-xs'>
                    <Link className='header-menu-link' href='/materials'>Materials</Link>
                </li>
                <li className='header-menu-element about hidden show-xs'>
                    <Link className='header-menu-link' href='/about'>About us</Link>
                </li>
            </ul>
        </header>
    );
}

export default Header;
