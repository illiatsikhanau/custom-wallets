import '@/styles/Checkbox.scss';
import {ReactNode} from 'react';

interface Props {
    children?: ReactNode
    required?: boolean
}

const Checkbox = (props: Props) => {
    return (
        <div className='checkbox-wrapper'>
            <label className='checkbox-label'>
                <input className='checkbox' type='checkbox' required={props.required}/>
                <span className='checkbox-mark'/>
            </label>
            <span>{props.children}</span>
        </div>
    );
}

export default Checkbox;
