import '@/styles/Input.scss';

interface Props {
    className?: string
    label?: string
    type?: 'text' | 'email'
    required?: boolean
}

const Input = (props: Props) => {
    return (
        <label className={`input-wrapper ${props.className ?? ''}`}>
            <span className='wrapper-label'>{props.label}</span>
            <input className='input-field' type={props.type} required={props.required}/>
        </label>
    );
}

export default Input;
