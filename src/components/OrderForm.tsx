'use client'

import '@/styles/OrderForm.scss';
import {FormEvent} from 'react';
import {useRouter} from 'next/navigation';
import Link from 'next/link';
import Button from '@/components/Button';
import Input from '@/components/Input';
import TextArea from '@/components/TextArea';
import Checkbox from '@/components/Checkbox';

const OrderForm = () => {
    const {push} = useRouter();

    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        push('/thanks');
    }

    return (
        <form className='order-form' onSubmit={onSubmit}>
            <h2 className='order-form-title m-1'>Order Form</h2>
            <Input className='w-full' label='Your Name' required/>
            <Input className='w-full' label='Your Phone Number' required/>
            <Input className='w-full' label='Your Email' type='email' required/>
            <TextArea className='w-full' label='Comment'/>
            <Checkbox required>
                I accept <Link className='order-form-link' href='/policy'>Privacy policy</Link>
            </Checkbox>
            <Button className='w-full'>
                Submit
            </Button>
        </form>
    );
}

export default OrderForm;
