'use client'

import '@/styles/Button.scss';
import {ReactNode} from 'react';
import {useRouter} from 'next/navigation';

interface Props {
    children?: ReactNode
    className?: string
    onClick?: () => void
    href?: string
}

const Button = (props: Props) => {
    const {push} = useRouter();

    const onClick = () => {
        props.onClick && props.onClick();
        props.href && push(props.href);
    }

    return (
        <button className={`button ${props.className ?? ''}`} onClick={onClick}>
            {props.children}
        </button>
    );
}

export default Button;
