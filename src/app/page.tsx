import '@/styles/HomePage.scss';
import Link from 'next/link';
import Button from '@/components/Button';
import OrderForm from '@/components/OrderForm';

const HomePage = () => {
    return (
        <div className='home-page'>
            <div className='banner sm' style={{backgroundImage: `url('/assets/banner1.webp')`}}>
                <div className='banner-filter justify-center'>
                    <h1>Explore custom-made<br/>leather wallets and accessories</h1>
                    <Button href='/#order-form'>Order Now</Button>
                </div>
            </div>
            <div className='banner-row'>
                <div className='banner-wrapper'>
                    <Link
                        className='banner sm'
                        href='/materials'
                        style={{backgroundImage: `url('/assets/banner2.webp')`}}
                    >
                        <div className='banner-filter justify-center'>
                            <h2>Premium Materials</h2>
                            <p>Exquisite selection for timeless elegance</p>
                        </div>
                    </Link>
                </div>
                <div className='banner-wrapper'>
                    <Link className='banner sm' href='/collection'
                          style={{backgroundImage: `url('/assets/banner3.webp')`}}>
                        <div className='banner-filter justify-center'>
                            <h2>Our Collection</h2>
                            <p>Handcrafted Leather Goods for Every Style</p>
                        </div>
                    </Link>
                </div>
            </div>
            <div className='banner lg parallax' style={{backgroundImage: `url('/assets/banner11.webp')`}}>
                <div className='banner-filter justify-center'>
                    <h2 className='m-half'>Crafting Artisanal Excellence</h2>
                    <p className='m-half'>Handcrafted Leather Goods for Every Style</p>
                    <Button href='/crafting'>Explore Now</Button>
                </div>
            </div>
            <div className='banner lg parallax' style={{backgroundImage: `url('/assets/banner4.webp')`}}>
                <div className='banner-filter justify-center'>
                    <h2 className='m-half'>Memento mori coasters</h2>
                    <p className='m-half'>4 pack</p>
                    <p className='m-half'>Available from Dec, 1st</p>
                    <Button href='/#order-form'>Pre-Order Now</Button>
                </div>
            </div>
            <div className='banner lg parallax' style={{backgroundImage: `url('/assets/banner6.webp')`}}>
                <div className='banner-filter justify-center'>
                    <Link href='/about' className='transition-block'>
                        <p>Our workshop combines the artistry of handcrafting with the use of high-quality materials to
                            create a unique and exquisite product</p>
                    </Link>
                    <Link href='/about' className='transition-block'>
                        <p>Our designers and artisans are committed to creating beauty and perfection in every
                            detail.</p>
                    </Link>
                    <Button href='/about'>Read Our Story</Button>
                </div>
            </div>
            <div className='banner lg parallax' style={{backgroundImage: `url('/assets/banner5.webp')`}}>
                <div className='banner-filter justify-center'>
                    <div id='order-form' className='order-form-wrapper'>
                        <OrderForm/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HomePage;
