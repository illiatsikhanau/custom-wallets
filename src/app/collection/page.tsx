import '@/styles/Catalog.scss';
import {Metadata} from 'next';
import Product from '@/components/Product';

export const metadata: Metadata = {
    title: 'Explore Our Exquisite Collection',
    description: 'Explore our curated collection, a testament to timeless elegance and superior craftsmanship. Discover a range of meticulously crafted products, each embodying our commitment to enduring style and artisanal excellence.',
}

const products = [
    {id: 1, src: '/assets/product1.webp', name: 'Traditional Wallet - English Tan'},
    {id: 2, src: '/assets/product2.webp', name: 'Traditional Wallet - Natural'},
    {id: 3, src: '/assets/product3.webp', name: 'Traditional Wallet - Black'},
    {id: 4, src: '/assets/product4.webp', name: 'Money Clip Wallet - English Tan'},
    {id: 5, src: '/assets/product5.webp', name: 'Money Clip Wallet - Natural'},
    {id: 6, src: '/assets/product6.webp', name: 'Money Clip Wallet - Black'},
    {id: 7, src: '/assets/product7.webp', name: 'Biker Wallet - English Tan'},
    {id: 8, src: '/assets/product8.webp', name: 'Biker Wallet - Natural'},
    {id: 9, src: '/assets/product9.webp', name: 'Biker Wallet - Black'},
    {id: 10, src: '/assets/product10.webp', name: 'Coin Wallet - English Tan'},
    {id: 11, src: '/assets/product11.webp', name: 'Coin Wallet - Natural'},
    {id: 12, src: '/assets/product12.webp', name: 'Coin Wallet - Black'},
    {id: 13, src: '/assets/product13.webp', name: 'Apple Watch Band - English Tan'},
    {id: 14, src: '/assets/product14.webp', name: 'Apple Watch Band - Natural'},
    {id: 15, src: '/assets/product15.webp', name: 'Apple Watch Band - Black'},
    {id: 16, src: '/assets/product16.webp', name: 'A5 Notebook Cover - English Tan'},
    {id: 17, src: '/assets/product17.webp', name: 'A5 Notebook Cover - Natural'},
    {id: 18, src: '/assets/product18.webp', name: 'A5 Notebook Cover - Black'},
    {id: 19, src: '/assets/product19.webp', name: 'Sunglasses Case'},
    {id: 20, src: '/assets/product20.webp', name: 'The Note Keeper - A5 Notebook'},
    {id: 21, src: '/assets/product21.webp', name: 'Your Custom Designed Product'},
];

const CollectionPage = () => {
    return (
        <div className='catalog'>
            <div className='banner' style={{backgroundImage: `url('/assets/banner7.webp')`}}>
                <div className='banner-filter'>
                    <h1>Explore Our Exquisite Collection</h1>
                    <p>Discover the Elegance of Fine Leather Creations</p>
                </div>
            </div>
            <div className='catalog-list'>
                {products.map(product =>
                    <div key={product.id} className='product-wrapper'>
                        <Product product={product}/>
                    </div>
                )}
            </div>
        </div>
    );
}

export default CollectionPage;
