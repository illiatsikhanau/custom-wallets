import '@/styles/Catalog.scss';
import {Metadata} from 'next';
import Product from '@/components/Product';

const products = [
    {id: 1, src: '/assets/material1.webp', name: 'Yufta Brown'},
    {id: 2, src: '/assets/material2.webp', name: 'Yufta Green'},
    {id: 3, src: '/assets/material3.webp', name: 'Yufta Dark Chestnut'},
    {id: 4, src: '/assets/material4.webp', name: 'Yufta Natural'},
    {id: 5, src: '/assets/material5.webp', name: 'Leather Black Reverse'},
    {id: 6, src: '/assets/material6.webp', name: 'Yufta White-Nights Loft'},
    {id: 7, src: '/assets/material7.webp', name: 'Yufta Solar Loft Without Stove'},
    {id: 8, src: '/assets/material9.webp', name: 'Leather Belatrissa Haberdashery'},
    {id: 9, src: '/assets/material9.webp', name: 'Leather Haberdashery Crust Cognac'},
    {id: 10, src: '/assets/material10.webp', name: 'Yufta Red + Reptile'},
    {id: 11, src: '/assets/material11.webp', name: 'Yufta Blue'},
    {id: 12, src: '/assets/material12.webp', name: 'Leather Haberdashery Black Tef'},
];

export const metadata: Metadata = {
    title: 'Fine Materials for Artisanal Creations',
    description: 'Discover our carefully curated selection of premium materials, embodying the essence of timeless craftsmanship. From exquisite leathers to refined metals, each element is meticulously chosen to reflect our commitment to unparalleled quality and enduring sophistication.',
}

const MaterialsPage = () => {
    return (
        <div className='catalog'>
            <div className='banner' style={{backgroundImage: `url('/assets/banner10.webp')`}}>
                <div className='banner-filter'>
                    <h1>Fine Materials for Artisanal Creations</h1>
                    <p>Crafting with Uncompromising Quality</p>
                </div>
            </div>
            <div className='catalog-list'>
                {products.map(product =>
                    <div key={product.id} className='product-wrapper'>
                        <Product product={product}/>
                    </div>
                )}
            </div>
        </div>
    );
}

export default MaterialsPage;
