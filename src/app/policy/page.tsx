import '@/styles/PolicyPage.scss';
import {Metadata} from 'next';
import Link from 'next/link';

export const metadata: Metadata = {
    title: 'Privacy Policy',
    description: 'Please review our comprehensive Privacy Policy and Cookie Policy to understand how we collect, use, and protect your data. Your privacy and browsing experience are important to us.',
}

const PolicyPage = () => {
    const url = 'custom-wallets.vercel.app';

    return (
        <div className='policy-page'>
            <h1>Privacy Policy</h1>
            <p className='policy-paragraph'>
                This Privacy Policy explains how Custom Wallets collects, uses, shares, and protects your
                personal information when you use our
                website <Link className='policy-link' href='/'>{url}</Link> and
                any related services. By accessing or using our services, you consent to the practices described
                in this policy.
            </p>
            <p className='policy-header'>Information We Collect</p>
            <p className='policy-paragraph'>
                We collect information that you provide directly to us, such as when you fill out forms,
                register for an account, make a purchase, or contact us. This information may include your name,
                email address, postal address, phone number, and other information you choose to provide.
            </p>
            <p className='policy-paragraph'>
                We may also collect information automatically when you use our website. This information may
                include your IP address, browser type, operating system, referral source, pages you viewed, and
                the dates and times of your visits.
            </p>
            <p className='policy-header'>How We Use Your Information</p>
            <p className='policy-paragraph'>
                We may use your information for various purposes, including:
            </p>
            <p className='policy-paragraph'>
                Providing and maintaining our website. Personalizing your experience on our website. Processing
                transactions and sending transaction notifications. Responding to your inquiries, comments, or
                questions. Sending you marketing and promotional communications, if you've opted in to receive
                them. Conducting research and analysis to improve our services.
            </p>
            <p className='policy-header'>Information Sharing and Disclosure</p>
            <p className='policy-paragraph'>
                We may share your personal information with:
            </p>
            <p className='policy-paragraph'>
                Service providers who help us operate, maintain, and improve our website. Third parties with
                whom we have contracts to provide specific services, such as payment processors, email service
                providers, and analytics tools. Legal authorities, if required by law or if we believe that
                disclosure is necessary to protect our rights, your safety, or the safety of others.
            </p>
            <p className='policy-header'>Data Security</p>
            <p className='policy-paragraph'>
                We take reasonable steps to protect your personal information from unauthorized access, use,
                alteration, or disclosure. However, no method of transmission over the internet or electronic
                storage is entirely secure, and we cannot guarantee absolute security.
            </p>
            <p className='policy-header'>Third-Party Links</p>
            <p className='policy-paragraph'>
                Our website may contain links to third-party websites or services that we do not operate or
                control. This Privacy Policy does not apply to those third-party websites or services, so we
                encourage you to review their respective privacy policies.
            </p>
            <p className='policy-header'>Changes to This Privacy Policy</p>
            <p className='policy-paragraph'>
                We may update our Privacy Policy to reflect changes in our practices or for other operational,
                legal, or regulatory reasons. We will notify you of any changes by posting the updated policy on
                this page with a new effective date.
            </p>
            <h2 id='cookie'>Cookie Policy</h2>
            <p className='policy-paragraph'>
                This Cookie Policy explains how Custom Wallets uses cookies and similar technologies to recognize you
                when you visit our website. It explains what these technologies are and why we use them, as well as your
                rights to control our use of them.
            </p>
            <p className='policy-header'>What are Cookies</p>
            <p className='policy-paragraph'>
                Cookies are small pieces of text sent by your web browser by a website you visit. A cookie file is
                stored in your web browser and allows the website or a third party to recognize you and make your next
                visit easier and the website more useful to you.
            </p>
            <p className='policy-header'>How We Use Cookies</p>
            <p className='policy-paragraph'>
                When you use and access our website, we may place a number of cookies files in your web browser. We use
                cookies for the following purposes:
            </p>
            <p className='policy-paragraph'>
                To enable certain functions of the website
            </p>
            <p className='policy-paragraph'>
                To provide analytics
            </p>
            <p className='policy-paragraph'>
                To store your preferences
            </p>
            <p className='policy-paragraph'>
                Third-Party Cookies
            </p>
            <p className='policy-paragraph'>
                In addition to our own cookies, we may also use various third-party cookies to report usage statistics
                of the website and deliver advertisements on and through the website.
            </p>
            <p className='policy-header'>What are Your Choices Regarding Cookies</p>
            <p className='policy-paragraph'>
                If you'd like to delete cookies or instruct your web browser to delete or refuse cookies, please visit
                the help pages of your web browser. Please note, however, that if you delete cookies or refuse to accept
                them, you might not be able to use all the features we offer, you may not be able to store your
                preferences, and some of our pages might not display properly.
            </p>
            <p className='policy-header'>Where You Can Find More Information About Cookies</p>
            <p className='policy-paragraph'>
                You can learn more about cookies and the following third-party websites:
            </p>
            <p className='policy-paragraph'>
                AllAboutCookies: <a className='policy-link' href='http://www.allaboutcookies.org'>
                allaboutcookies.org</a>
            </p>
            <p className='policy-paragraph'>
                Network Advertising Initiative: <a className='policy-link' href='http://www.networkadvertising.org'>
                networkadvertising.org</a>
            </p>
            <p className='policy-header'>Contact Us</p>
            <p className='policy-paragraph'>
                If you have any questions about this Privacy Policy or your personal information, please contact
                us at <a className='policy-link' href={`mailto:support@${url}`}>support@{url}</a>
            </p>
            <p className='policy-header'>Last Updated: 26.10.2023</p>
        </div>
    );
}

export default PolicyPage;
