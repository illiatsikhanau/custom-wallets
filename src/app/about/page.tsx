import '@/styles/AboutPage.scss';
import {Metadata} from 'next';

export const metadata: Metadata = {
    title: 'About Us',
    description: 'Discover the story behind Custom Wallets, where passion and craftsmanship unite. Learn about our commitment to timeless elegance and superior quality.',
}

const AboutPage = () => {
    const address = 'Helsinki, Senate Square';
    const phone = '+358-0-000-000';
    const email = 'support@custom-wallets.vercel.app';

    return (
        <div className='about-page'>
            <h1>About Us</h1>
            <p className='about-paragraph'>
                Founded in the heart of our town several decades ago, our workshop has stood as a testament to the
                enduring tradition of fine leather craftsmanship. What began as a modest venture by a passionate artisan
                has evolved into a renowned establishment, revered for its dedication to producing exquisite leather
                goods. From the early days of humble beginnings to the present, our workshop has remained steadfast in
                its commitment to preserving the artistry and elegance inherent in each handcrafted piece.
            </p>
            <p className='about-paragraph'>
                Through the passage of time, our workshop has weathered various challenges, emerging stronger and more
                resilient with each experience. Generations of skilled artisans have passed down their expertise,
                cultivating a legacy of precision and artisanship that defines our brand today. With a rich history
                steeped in the pursuit of perfection, our workshop continues to uphold the values of integrity, quality,
                and timeless craftsmanship that have been the cornerstone of our success.
            </p>
            <p className='about-paragraph'>
                From the intricate detailing of our earliest creations to the innovative techniques employed in our
                modern designs, our workshop's history is etched into every stitch and fold of the leather goods we
                produce. As we look back on our journey, we take pride in our heritage and look forward to a future
                defined by the same unwavering dedication to excellence that has guided us throughout the years.
            </p>
            <p className='about-paragraph'>
                Address Location: <a className='about-link'
                                     href='https://www.google.com/maps/place/Senate+Square/@60.169479,24.952287,16z/data=!4m6!3m5!1s0x46920bce46c319b7:0xbc57e32cb4d908ca!8m2!3d60.1694794!4d24.9522867!16zL20vMDZsYmtk'>{address}</a>
            </p>
            <p className='about-paragraph'>
                Phone number: <a className='about-link' href={`tel:${phone}`}>{phone}</a>
            </p>
            <p className='about-paragraph'>
                Postal address: <a className='about-link' href={`mailto:${email}`}>{email}</a>
            </p>
            <iframe className='map' title='Address Location' width='100%' height='500' allowFullScreen loading='lazy'
                    referrerPolicy='no-referrer-when-downgrade'
                    src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1983.6732715680712!2d24.952286999999984!3d60.16947899999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46920bce46c319b7%3A0xbc57e32cb4d908ca!2sSenate%20Square!5e0!3m2!1sen!2sby!4v1693946748449!5m2!1sen!2sby'/>
        </div>
    );
}

export default AboutPage;
