import '../../public/globals.css';
import {ReactNode} from 'react';
import type {Metadata} from 'next';
import {Inter} from 'next/font/google';
import Header from '@/components/Header';
import Footer from '@/components/Footer';
import CookieAlert from '@/components/CookieAlert';

const inter = Inter({subsets: ['latin']})

export const metadata: Metadata = {
    title: 'Create Your Own Personalized Wallets',
    description: 'Explore custom-made leather wallets and accessories. Our skilled artisans meticulously handcraft high-quality, personalized leather with premium materials.',
}

export default function RootLayout({children}: { children: ReactNode }) {
    return (
        <html lang='en'>
        <body className={inter.className}>
        <Header/>
        {children}
        <Footer/>
        <CookieAlert/>
        </body>
        </html>
    );
}
