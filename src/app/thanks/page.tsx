import '@/styles/ThanksPage.scss';

const ThanksPage = () => {
    return (
        <div className='thanks-page'>
            <div className='banner-filter'>
                <div className='message'>
                    <h1>Thank you for choosing us!</h1>
                    <p>
                        We appreciate your order and your interest in our luxury
                        leather goods. Our team is committed to ensuring your complete satisfaction. You can expect a
                        callback or confirmation of your order shortly. We look forward to serving you and hope you
                        will enjoy your purchase.
                    </p>
                </div>
            </div>
        </div>
    );
}

export default ThanksPage;
