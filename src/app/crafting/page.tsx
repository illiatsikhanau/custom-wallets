import '@/styles/CraftingPage.scss';
import {Metadata} from 'next';
import Button from '@/components/Button';

export const metadata: Metadata = {
    title: 'Crafting Artisanal Excellence',
    description: 'Experience the art of meticulous craftsmanship, where every detail reflects our dedication to timeless artistry and innovation. Discover the process behind each masterpiece as our artisans bring passion and precision to every creation.',
}

const CraftingPage = () => {
    return (
        <div className='crafting-page'>
            <h1 className='hidden'>Crafting Artisanal Excellence</h1>
            <div className='banner' style={{backgroundImage: `url('/assets/banner6.webp')`}}>
                <div className='banner-filter'>
                    <h2>The best masters</h2>
                    <p>Our masters embody a legacy of exceptional craftsmanship, infusing each creation with artistry
                        and timeless elegance. With every detail, their expertise and passion shine, defining each piece
                        as a masterpiece of unparalleled artisanship.</p>
                </div>
            </div>
            <div className='banner' style={{backgroundImage: `url('/assets/banner8.webp')`}}>
                <div className='banner-filter'>
                    <h2>Exquisite Materials</h2>
                    <p>Crafted with the finest materials, our pieces embody enduring quality and elegance, ensuring
                        timeless sophistication and lasting durability.</p>
                    <Button href='/materials'>Explore</Button>
                </div>
            </div>
            <div className='banner' style={{backgroundImage: `url('/assets/banner9.webp')`}}>
                <div className='banner-filter'>
                    <h2>Exemplary Quality, Timeless Sophistication</h2>
                    <p>Our commitment to excellence is unwavering. Each piece is meticulously crafted with the finest
                        materials, reflecting our dedication to enduring quality and timeless sophistication.</p>
                    <Button href='/collection'>Explore</Button>
                </div>
            </div>
        </div>
    );
}

export default CraftingPage;
